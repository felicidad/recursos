const Hoek = require('@hapi/hoek');
const Generator = require('./lib/generator');
const Store = require('./lib/store');


module.exports = {
    name: 'recursos',
    register: function (server, options) {

        const store = Store({

            models: new Map(),
            queries: new Map()
        });

        server.bind(store);
        server.decorate('server', 'recursos:store', () => {

            return store;
        });

        // Accepts an objects of models where { [modelName]: Model }
        // Accepts an object of configs where { [modelName]: { ...config } }
        const resources = function (models, configs = {}, opts = {}) {

            Hoek.assert(models, 'Models is required');
            Hoek.assert(typeof models.query !== 'function', 'Models cannot be a single objection model. Must pass an object of models.');

            const modelKeys = Object.keys(models);
            const itxKeys = Hoek.intersect(modelKeys, Object.keys(configs));

            Hoek.assert(
                modelKeys.length === itxKeys.length,
                'Each model requires a config of same name'
            );

            Object.entries(models).forEach(([name, model]) => {

                const config = configs[name];
                server.resource(name, model, config, opts);
            });
        };

        server.decorate('server', 'resource', Generator);
        server.decorate('server', 'resources', resources);
    }
};
