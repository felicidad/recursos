/* eslint-disable @hapi/hapi/no-arrowception */
// NPM Modules
const Path = require('path');
const Fs = require('fs');
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');
const Hapi = require('@hapi/hapi');
const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

// App Modules
const Routes = require('../lib/routes');

// Module Variables
const {
    experiment,
    it,
    before,
    after,
    beforeEach,
    afterEach
} = exports.lab = Lab.script();

const expect = Code.expect;
const msg = (...args) => args.join(' ');


const noop = (val) => (() => val);
const noprom = (val) => () => Promise.resolve(val);

const stub = {

    handlers: {},
    config: {},
    mockModel: {

        createSchema: Joi.object().keys({
            create: Joi.string().required()
        }),

        updateSchema: Joi.object().keys({
            update: Joi.string().required()
        }),

        relationMappings: {
            posts: {},
            user: {},
            image: {}
        }
    },
    queries: require('./queries.json'),
    qFns: {}
};

stub.queries.forEach((key) => (stub.qFns[key] = noprom(key)));


stub.server = Hapi.Server({
    port: 4321,
    routes: {
        validate: {
            failAction: (request, h, err) => {

                if (err) {

                    throw Boom.badRequest(err.message);
                }

                return h.continue;
            }
        }
    }
});

experiment('Handlers CRUD',  () => {

    // experiment('defaults',  () => {
    // });
});
