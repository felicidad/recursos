// NPM Modules
const Path = require('path');
const Fs = require('fs');
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');
const Hapi = require('@hapi/hapi');
const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

// App Modules
const Routes = require('../lib/routes');

// Module Variables
const {
    experiment,
    it,
    before,
    after,
    beforeEach,
    afterEach
} = exports.lab = Lab.script();

const expect = Code.expect;
const msg = (...args) => args.join(' ');
const noop = (val) => () => val;
const noprom = (val) => () => Promise.resolve(val);

const stub = {

    handlers: {},
    config: {},
    mockModel: {

        createSchema: Joi.object().keys({
            create: Joi.string().required()
        }),

        updateSchema: Joi.object().keys({
            update: Joi.string().required()
        }),

        relationMappings: {
            posts: {},
            user: {},
            image: {}
        }
    },
    queries: require('./queries.json')
};

stub.queries.forEach((key) => stub.handlers[key] = noprom(key));

const mapRoutes = () => {

    // Grab all routes from the route table
    const routes = stub.server.table()

        .map(({

            // We will inspect the things we set
            method,
            params,
            path,
            settings: {
                validate,
                description,
                tags,
                handler,
                notes,
                auth
            }
        }) =>

            ({
                method,
                path,
                params,
                validate,
                description,
                tags,
                handler,
                notes,
                auth
            })
        )

        // Reduce into object
        .reduce((acc, cur) => {

            try {
                const name = cur.tags[1];

                acc[name] = cur;
                return acc;
            }
            catch (e) {

                throw e;
            }
        }, {});

    stub.routes = routes;
}

const beforeFn = () => {

    stub.server = Hapi.Server({
        port: 4321,
        routes: {
            validate: {
                failAction: (request, h, err) => {

                    if (err) {

                        throw Boom.badRequest(err.message);
                    }

                    return h.continue;
                }
            }
        }
    });


    stub.server.auth.scheme('api', () => ({ authenticate: noop(true) }));
    stub.server.auth.strategy('api', 'api');
}

const joiByKey = (key) => Joi.object().keys({
    test: Joi.string().allow(key)
});

experiment('Routes CRUD',  () => {

    experiment('defaults',  () => {

        const plural = 'users';
        const singular = 'user';

        before(async () => {

            beforeFn();

            const config = {
                singular: 'user',
                plural: 'users',
                name: 'User'
            };



            stub.server.route(
                Routes.Crud.bind(stub.server)(stub.handlers, config)
            );

            mapRoutes();
        });

        it('should have default events', () => {

            expect(
                Object.keys(
                    stub.server.events._eventListeners
                )
            ).to.include([
                'User:updateWhere',
                'User:delWhere',
                'User:insert',
                'User:updateOne',
                'User:delOne'
            ]);

        })


        // Must be tagged the plural name of model
        Object
            .values(stub.queries)
            .forEach((key) => {

                it(`${key}: has model resources with proper route configuration`, () => {

                    const { routes, queries } = stub;
                    const { tags, handler, description } = routes[key];

                    // Expect all queries to exist
                    expect(routes).to.include(queries);

                    // Should have tags
                    expect(tags, msg('tags', key)).to.include([plural, key]);

                    // Should have a handler
                    expect(handler, msg('handler', key)).to.be.a.function();

                    // Should have a description
                    expect(description, msg('description', key)).to.be.a.string();
                    expect(description.length, msg('description ln', key)).to.be.above(1);

                });
            });

        // These routes should have ID params
        [
            'findById',
            'delOne',
            'updateOne'
        ].forEach((key) => {

            it(`${key}: should have id params`, () => {

                expect(stub.routes[key].params, msg('params', key)).to.include('id')
            })
        });

        // Assert query validation
        [
            'all',
            'allWhere'
        ].forEach((key) => {

            it(`${key}: should have default query validation`, () => {

                const { validate } = stub.routes[key];

                expect(validate.query, msg('query', key)).to.be.object();
                expect(validate.query.isJoi, msg('query', key)).to.be.true();
            });
        });

        // Assert payload validation
        [
            'allWhere',
            'updateWhere',
            'delWhere',
            'countWhere',
            'oneWhere',
            'insert',
            'updateOne'
        ].forEach((key) => {

            it(`${key}: should have default payload validation`, () => {

                const { validate } = stub.routes[key];

                expect(validate.payload, msg('payload', key)).to.be.object();
                expect(validate.payload.isJoi, msg('payload', key)).to.be.true();
            });
        });

        // Assert params validation
        [
            'findById',
            'findByField',
            'delOne',
            'updateOne'
        ].forEach((key) => {

            it(`${key}: should have default params validation`, () => {

                const { validate } = stub.routes[key];

                expect(validate.params, msg('params', key)).to.be.object();
                expect(validate.params.isJoi, msg('params', key)).to.be.true();
            });
        });

        // Assert plural paths
        [
            'all',
            'allWhere',
            'updateWhere',
            'delWhere',
            'count',
            'countWhere',
        ].forEach((key) => {

            it(`${key}: should have default paths`, () => {

                const { path } = stub.routes[key];
                expect(path, msg('plural', key)).to.match(new RegExp(`/${plural}`));
            });
        });

        // Assert singular paths
        [
            'findById',
            'findByField',
            'oneWhere',
            'delOne',
            'insert',
            'updateOne'
        ].forEach((key) => {

            it(`${key}: should have default paths`, () => {

                const { path } = stub.routes[key];
                expect(path, msg('singular', key)).to.match(new RegExp(`/${singular}`));
            });
        });


        // Assert query validation
        [
            'all',
            'allWhere'
        ].forEach((key) => {

            it(`${key}: should reject invalid query input`, async () => {

                const { routes, server } = stub;
                const { method, path } = routes[key];

                const response = await server.inject({
                    method,
                    url: path + '?wat=0'
                });

                expect(response.statusCode, msg('query', key)).to.equal(400);
            })
        });

        [
            'allWhere',
            'delWhere',
            'updateWhere',
            'countWhere',
            'oneWhere',
            'insert',
            'updateOne'
        ].forEach((key) => {

            it(`${key}: should reject invalid payload input`, async () => {

                const { routes, server } = stub;
                const { method, path } = routes[key];

                const opts = {
                    method,
                    url: path,
                    payload: 'caca'
                };

                const strPayload = await server.inject({ ...opts });

                opts.payload = 13;
                const numPayload = await server.inject({ ...opts });

                opts.payload = undefined;
                const noPayload = await server.inject({ ...opts });

                expect(strPayload.statusCode, msg('payload', 'str', key)).to.equal(400);
                expect(numPayload.statusCode, msg('payload', 'num', key)).to.equal(400);
                expect(noPayload.statusCode, msg('payload', 'undefined', key)).to.equal(400);
            })
        });

        // Handle special updateWhere case
        it(`updateWhere: should reject invalid payload input`, async () => {

            const { routes, server } = stub;
            const { method, path } = routes.updateWhere;
            const key = 'updateWhere';

            const opts = {
                method,
                url: path,
                payload: 'blyat'
            };

            const strPayload = await server.inject({ ...opts });

            opts.payload = 13;
            const numPayload = await server.inject({ ...opts });

            opts.payload = undefined;
            const noPayload = await server.inject({ ...opts });

            opts.payload = [1];
            const arrPayload = await server.inject({ ...opts });

            expect(strPayload.statusCode, msg('payload', 'str', key)).to.equal(400);
            expect(numPayload.statusCode, msg('payload', 'num', key)).to.equal(400);
            expect(noPayload.statusCode, msg('payload', 'undefined', key)).to.equal(400);
            expect(arrPayload.statusCode, msg('payload', 'arr', key)).to.equal(400);

            opts.payload = { test: true };
            let objPayload = await server.inject({ ...opts });

            expect(objPayload.statusCode, msg('payload', 'obj', key)).to.equal(400);

            opts.payload = { where: [] };
            objPayload = await server.inject({ ...opts });

            expect(objPayload.statusCode, msg('payload', 'obj no changeset', key)).to.equal(400);

            opts.payload = { changeset: { test: true } };
            objPayload = await server.inject({ ...opts });

            expect(objPayload.statusCode, msg('payload', 'obj no where', key)).to.equal(400);

            opts.payload = { where: [], changeset: 'blyat'  };
            objPayload = await server.inject({ ...opts });

            expect(objPayload.statusCode, msg('payload', 'obj bad changeset', key)).to.equal(400);

            opts.payload = { where: 'blyat', changeset: { test: true } };
            objPayload = await server.inject({ ...opts });

            expect(objPayload.statusCode, msg('payload', 'obj bad where', key)).to.equal(400);

        });

        // Assert params validation
        [
            'findById',
            // 'findByField',
            'delOne',
            'updateOne'
        ].forEach((key) => {

            it(`${key}: should reject invalid params input`, async () => {

                const { routes, server } = stub;
                const { method, path } = routes[key];

                const opts = {
                    method,
                    url: path
                        .replace('{id}', 'test')
                        .replace('{field}', '1')
                        .replace('{value}', '1')
                };

                const response = await server.inject({ ...opts });

                expect(response.statusCode, msg('params', 'str', key)).to.equal(400);
            })
        });

        // Assert query validation
        [
            'all',
            'allWhere'
        ].forEach((key) => {

            it(`${key}: should accept valid query input`, async () => {

                const { routes, server } = stub;
                const { method, path } = routes[key];

                const response = await server.inject({
                    method,
                    url: path + '?page=1&limit=200',
                    payload: [1]
                });

                expect(response.statusCode, msg('query', key)).to.equal(200);
            });
        });

        // Assert payload validation
        [
            'allWhere',
            'delWhere',
            'countWhere',
            'oneWhere',
            'insert',
            'updateOne'
        ].forEach((key) => {

            it(`${key}: should accept valid payload input`, async () => {

                const { routes, server } = stub;
                const { method, path } = routes[key];

                const opts = {
                    method,
                    url: path,
                    payload: ['array']
                };

                if (['oneWhere', 'updateOne'].includes(key)) {
                    opts.url = opts.url.replace('{id}', '1');
                }

                const arrPayload = await server.inject({ ...opts });

                opts.payload = { object: true };
                const objPayload = await server.inject({ ...opts });

                expect(arrPayload.statusCode, msg('payload', 'arr', key)).to.equal(200);
                expect(objPayload.statusCode, msg('payload', 'obj', key)).to.equal(200);
            })
        })


        // Handle special updateWhere case
        it(`updateWhere: should accept valid route input`, async () => {

            const { routes, server } = stub;
            const { method, path } = routes.updateWhere;
            const key = 'updateWhere';

            const opts = {
                method,
                url: path,
                payload: {
                    where: ['array'],
                    changeset: { pass: true }
                }
            };

            const arrPayload = await server.inject({ ...opts });

            opts.payload.where = { object: true };
            const objPayload = await server.inject({ ...opts });

            expect(arrPayload.statusCode, msg('payload', 'arr', key)).to.equal(200);
            expect(objPayload.statusCode, msg('payload', 'obj', key)).to.equal(200);
        });

        // Assert params validation
        [
            'findById',
            'findByField',
            'delOne',
            'updateOne'
        ].forEach((key) => {

            it(`${key}: should accept valid route input`, async () => {

                const { routes, server } = stub;
                const { method, path } = routes[key];

                const opts = {
                    method,
                    url: path
                        .replace('{id}', '1')
                        .replace('{field}', 'slug')
                        .replace('{value}', 'test'),
                    payload: { test: true }
                };

                const response = await server.inject({ ...opts });

                expect(response.statusCode, msg('params', 'str', key)).to.equal(200);
            });
        });

        const testPayloads = {
            default: { test: true },
            updateWhere: { where: [1], changeset: { test:true } }
        };

        Object.values(stub.queries)
            .forEach((key) => {

                it(`${key}: should call appropriate queries`, async () => {

                    const { routes, server } = stub;
                    const { path, method } = routes[key];

                    const opts = {
                        method,
                        url: path,
                    };

                    if ([
                        'findById',
                        'updateOne',
                        'delOne'
                    ].includes(key)) {
                        opts.url = opts.url
                            .replace('{id}', '1')
                            .replace('{field}', 'slug')
                            .replace('{value}', 'test');
                    }

                    if (['updateWhere'].includes(key)) {
                        opts.payload = testPayloads[key];
                    }
                    else {
                        opts.payload = testPayloads.default
                    }

                    const response = await server.inject(opts);

                    expect(response.result, key).to.equal(key);
                    expect(response.statusCode, key).to.equal(200);
                });
            });

    });

    experiment('overwrite config',  () => {

        beforeEach(async () => {

            beforeFn();
        });

        Object.entries({
            // Add a universal config for options like auth schemas
            // '*': {
            //     options: {
            //         auth: 'pinga'
            //     }
            // },
            all: {
                options: { notes: 'all' },
                validate: {
                    query: joiByKey('all')
                },
                paths: '/all',
            },
            allWhere: {
                options: { notes: 'allWhere' },
                validate: {
                    payload: joiByKey('allWhere'),
                    query: joiByKey('allWhere')
                },
                paths: '/allWhere',
            },
            updateWhere: {
                options: { notes: 'updateWhere' },
                validate: {
                    payload: joiByKey('updateWhere'),
                    query: joiByKey('updateWhere')
                },
                paths: '/updateWhere'
            },
            delWhere: {
                options: { notes: 'delWhere' },
                validate: {
                    payload: joiByKey('delWhere'),
                    query: joiByKey('delWhere')
                },
                paths: '/delWhere'
            },
            count: {
                options: { notes: 'count' },
                validate: {
                    query: joiByKey('count')
                },
                paths: '/count',
            },
            countWhere: {
                options: { notes: 'countWhere' },
                validate: {
                    payload: joiByKey('countWhere'),
                    query: joiByKey('countWhere')
                },
                paths: '/countWhere',
            },
            findById: {
                options: { notes: 'findById' },
                validate: {
                    query: joiByKey('findById')
                },
                paths: '/findById/{slug}',
            },
            findByField: {
                options: { notes: 'findByField' },
                validate: {
                    query: joiByKey('findByField')
                },
                paths: '/findByField/{field}',
            },
            oneWhere: {
                options: { notes: 'oneWhere' },
                validate: {
                    query: joiByKey('oneWhere')
                },
                paths: '/oneWhere',
            },
            insert: {
                options: { notes: 'insert' },
                validate: {
                    query: joiByKey('insert')
                },
                paths: '/insert',
            },
            updateOne: {
                options: { notes: 'updateOne' },
                validate: {
                    payload: joiByKey('updateOne'),
                    query: joiByKey('updateOne')
                },
                paths: '/updateOne/{slug}'
            },
            delOne: {
                options: { notes: 'delOne' },
                validate: {
                    payload: joiByKey('delOne'),
                    query: joiByKey('delOne')
                },
                paths: '/delOne/{slug}'
            }
        }).forEach(([key, config]) => {

            it(`${key}: it overwrites configs`, () => {

                const overwrite = {};

                Object.entries(config)
                    .forEach(([k, val]) => {

                        overwrite[k] = { [key]: val }
                    });

                // Overrides will ignore model schemas
                stub.server.route(
                    Routes.Crud.bind(stub.server)(
                        stub.handlers, {
                            singular: 'user',
                            plural: 'users',
                            ...overwrite
                        },
                        {
                            model: stub.mockModel
                        }
                    )
                );

                mapRoutes();

                const { path, notes, validate } = stub.routes[key];
                const { payload, query } = validate.query;

                if (query && query.isJoi) {

                    expect(query.validate({ test: key }).error).to.be.null();
                }

                if (payload && payload.isJoi) {

                    expect(query.validate({ test: key }).error).to.be.null();
                }

                expect(config.paths).to.equal(path);
                expect(config.options.notes).to.equal(notes);
            });
        });

        it('should apply a config to all routes', () => {

            stub.server.route(
                Routes.Crud.bind(stub.server)(
                    stub.handlers, {
                        singular: 'user',
                        plural: 'users',
                        name: 'User',
                        prefix: '/api',
                        options: {
                            '*': {
                                auth: 'api',
                            }
                        },
                        events: {
                            '*': false
                        }
                    }
                )
            );

            mapRoutes();

            expect(
                Object.keys(
                    stub.server.events._eventListeners
                )
            ).to.not.include([
                'User:updateWhere',
                'User:delWhere',
                'User:insert',
                'User:updateOne',
                'User:delOne'
            ]);

            Object.values(stub.routes).forEach(({ auth, path }) => {

                expect(auth, 'auth').to.not.be.equal('api');
                expect(path, 'path prefix').to.match(/^\/api/);

            });

        });

        it('should overwrite default validation if defined in model', async () => {

            stub.server.route(
                Routes.Crud.bind(stub.server)(
                    stub.handlers, {
                        singular: 'user',
                        plural: 'users',
                        name: 'User',
                        config: {}
                    },
                    {
                        model: stub.mockModel
                    }
                )
            );

            mapRoutes();

            const { insert, updateOne, updateWhere } = stub.routes;

            let testInsert = insert.validate.payload.validate({ bad: true });
            let testInsertMany = insert.validate.payload.validate([{ bad: true }]);
            let testUpdateOne = updateOne.validate.payload.validate({ bad: true });
            let testUpdateWhere = updateWhere.validate.payload.validate({ bad: true });

            expect(testInsert.error, 'testInsert not null').to.not.be.null();
            expect(testInsertMany.error, 'testInsert not null').to.not.be.null();
            expect(testUpdateOne.error, 'testUpdateOne not null').to.not.be.null();
            expect(testUpdateWhere.error, 'testUpdateWhere not null').to.not.be.null();

            testInsert = insert.validate.payload.validate({ create: 'true' });
            testInsertMany = insert.validate.payload.validate([{ create: 'true' }]);

            testUpdateOne = updateOne.validate.payload.validate({ update: 'true' });
            testUpdateWhere = updateWhere.validate.payload.validate({
                where: ['x', 1],
                changeset: {
                    update: 'true'
                }
            });

            expect(testInsert.error, 'testInsert null').to.be.null();
            expect(testInsertMany.error, 'testInsert null').to.be.null();
            expect(testUpdateOne.error, 'testUpdateOne null').to.be.null();
            expect(testUpdateWhere.error, 'testUpdateWhere null').to.be.null();

        })
    });
});
