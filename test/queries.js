// NPM Modules
const Path = require('path');
const Fs = require('fs');
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');
const Hapi = require('@hapi/hapi');
const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

// App Modules
const Queries = require('../lib/queries');
const Db = require('./db');

// Module Variables
const {
    experiment,
    it,
    before,
    after,
    beforeEach,
    afterEach
} = exports.lab = Lab.script();

const expect = Code.expect;
const msg = (...args) => args.join(' ');
// const noop = (val) => () => val;
// const noprom = (val) => () => Promise.resolve(val);

const stub = {

    queries: [
        'all',
        'allWhere',
        'updateWhere',
        'delWhere',
        'count',
        'countWhere',
        'findById',
        'oneWhere',
        'updateOne',
        'delOne'
    ]
};

experiment('Queries', () => {

    experiment('CRUD', () => {

        before(async () => {

            stub.db = await Db();
            Object.assign(stub, Queries.Crud(stub.db.models.Post));
        });

        after(async () => {


            await stub.db.knex.migrate.rollback();
        });

        it('gets all posts', async () => {

            const posts = await stub.all();

            expect(posts.length).to.equal(8);
        });

        it('gets all posts paginated', async () => {

            const posts = await stub.all({ page: 0, limit: 2 });
            expect(posts).to.contain(['results', 'total']);
            expect(posts.results.length).to.equal(2);
            expect(posts.results[0].id).to.equal(1);
        });

        it('gets all posts ordered by', async () => {

            let posts = await stub.all({ orderBy: 'title' });
            expect(posts[0].title).to.equal('como bailar casino');
            expect(posts[7].title).to.equal('vamo a ver si el jefe sabe');

            posts = await stub.all({ orderBy: 'title', order: 'desc' });

            expect(posts[0].title).to.equal('vamo a ver si el jefe sabe');
            expect(posts[7].title).to.equal('como bailar casino');
        });

        it('gets all posts paginated and ordered by', async () => {

            const posts = await stub.all({ page: 0, limit: 2, orderBy: 'title' });
            expect(posts).to.contain(['results', 'total']);
            expect(posts.results[0].title).to.equal('como bailar casino');
            expect(posts.results[1].title).to.equal('como conseguir billete');
        });

        it('gets all posts where filter is an object', async () => {

            const posts = await stub.allWhere({ status: 'public' });
            expect(posts.length).to.equal(3);
        });

        it('gets all posts where filter is an array', async () => {

            const posts = await stub.allWhere(['status', 'friends']);
            expect(posts.length).to.equal(2);
        });

        it('gets all posts where paginated', async () => {

            const posts = await stub.allWhere(
                { status: 'public' },
                { page: 0, limit: 2 }
            );
            expect(posts).to.contain(['results', 'total']);
            expect(posts.results.length).to.equal(2);
        });

        it('gets all posts where ordered by', async () => {

            let posts = await stub.allWhere(
                { status: 'public' },
                { orderBy: 'title' }
            );

            expect(posts[0].title).to.equal('como conseguir billete');
            expect(posts[2].title).to.equal('como ser guapo');

            posts = await stub.allWhere(
                { status: 'public' },
                { orderBy: 'title', order: 'desc' }
            );

            expect(posts[0].title).to.equal('como ser guapo');
            expect(posts[2].title).to.equal('como conseguir billete');
        });


        it('gets all posts where ordered by', async () => {

            const posts = await stub.allWhere(
                { status: 'public' },
                { page: 0, limit: 2, orderBy: 'title' }
            );

            expect(posts).to.contain(['results', 'total']);
            expect(posts.results.length).to.equal(2);

            expect(posts.results[0].title).to.equal('como conseguir billete');
            expect(posts.results[1].title).to.equal('como no dejar q te cogan pal trajin');
        });


        it('updates posts where filter is an object', async () => {

            const posts = await stub.updateWhere({
                where: { status: 'friends' },
                changeset: { status: 'caca' }
            });

            expect(posts, 'update').to.equal(2);

            const updated = await stub.allWhere(['status', 'caca']);
            expect(updated.length, 'verify').to.equal(2);
        });

        it('updates posts where filter is an array', async () => {

            const posts = await stub.updateWhere({
                where: ['status', 'caca'],
                changeset: { status: 'coco' }
            });

            expect(posts, 'update').to.equal(2);

            const updated = await stub.allWhere(['status', 'coco']);
            expect(updated.length, 'verify').to.equal(2);
        });

        it('deletes posts where filter is an object', async () => {

            const posts = await stub.delWhere({
                status: 'delete'
            });

            expect(posts, 'deleted').to.equal(1);

            const updated = await stub.allWhere(['status', 'delete']);
            expect(updated.length, 'verify').to.equal(0);
        });

        it('deletes posts where filter is an array', async () => {

            const posts = await stub.delWhere(['status', 'daleet']);

            expect(posts, 'daleeted').to.equal(1);

            const updated = await stub.allWhere(['status', 'daleet']);
            expect(updated.length, 'verify').to.equal(0);
        });

        it('counts posts', async () => {

            const count = await stub.count();

            expect(count.total, 'count').to.equal('6');
        });

        it('counts posts where filter is object', async () => {

            const count = await stub.countWhere({ status: 'public' });
            expect(count.total, 'count').to.equal('3');
        });

        it('finds a post by id', async () => {

            const post = await stub.findById(2);
            expect(post.title).to.equal('como bailar casino');
        });

        it('finds a post where filter is an object', async () => {

            const post = await stub.oneWhere({ status: 'private', user_id: 2 });
            expect(post.title).to.equal('como estafar seguro medico');
        });

        it('finds a post where filter is an array', async () => {

            const post = await stub.oneWhere(['user_id', 2]);
            expect(post.title).to.equal('como conseguir billete');
        });

        it('updates a post by id', async () => {

            const post = await stub.updateOne(2, { status: 'updated' });
            expect(post).to.equal(1);
        });

        it('inserts one post', async () => {

            await stub.insert({

                title: 'como insertar un post',
                body: 'aprendete knexjs',
                status: 'created',
                user_id: 3
            });

            const post = await stub.oneWhere({ status: 'created', user_id: 3 });

            expect(post).to.exist();
            expect(post.id).to.equal(9);
        });

        it('deletes one post by id', async () => {

            const deleted = await stub.delOne(9);
            expect(deleted).to.equal(1);

            const post = await stub.findById(9);
            expect(post).to.not.exist();
        });
    });
})
;
