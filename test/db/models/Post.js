const { Model } = require('objection');
const Joi = require('@hapi/joi');

class Post extends Model {
    // Table name is the only required property.
    static get tableName() {

        return 'posts';
    }

    static get createSchema() {

        return Joi.object().keys({

        });
    }

    static get updateSchema() {

        return Joi.object().keys({

        });
    }

    // This object defines the relations to other models.
    static get relationMappings() {

        return {
            comments: {
                relation: Model.HasManyRelation,
                // The related model. This can be either a Model subclass constructor or an
                // absolute file path to a module that exports one.
                modelClass: './Comment',
                join: {
                    from: 'posts.id',
                    to: 'comments.post_id'
                }
            }
        };
    }
}

module.exports = Post;
