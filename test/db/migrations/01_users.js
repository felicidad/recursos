exports.up = function (knex, Promise) {

    return Promise.all([
        knex.schema.createTable('users', (table) => {

            table.increments('id').primary();
            table.string('firstName').notNullable();
            table.string('lastName').notNullable();
            table.string('status').notNullable();
            table.timestamps(true, true);
        })
    ]);
};

exports.down = function (knex, Promise) {

    return Promise.all([knex.schema.dropTable('users')]);
};
