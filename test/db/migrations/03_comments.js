exports.up = function (knex, Promise) {

    return Promise.all([
        knex.schema.createTable('comments', (table) => {

            table.increments('id').primary();
            table.string('body').notNullable();
            table.integer('user_id');
            table.integer('post_id');

            table.timestamps(true, true);
        })
    ]);
};

exports.down = function (knex, Promise) {

    return Promise.all([knex.schema.dropTable('comments')]);
};
