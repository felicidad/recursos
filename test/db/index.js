const Path = require('path');
const Knex = require('knex');
const { Model } = require('objection');
const { requireFiles } = require('wranch');

const stub = {
    modelsPath: Path.resolve(__dirname, 'models'),
    dbConfig: {

        client: 'pg',
        connection: {
            database: 'test',
            user: 'root',
            password: 'root',
            host: 'localhost',
            port: '5432'

        },
        migrations: {
            directory: Path.resolve(__dirname, 'migrations')
        },
        seeds: {
            directory: Path.resolve(__dirname, 'seeds')
        },
    }
};




module.exports = async () => {

    const knex = Knex(stub.dbConfig);
    Model.knex(knex);

    await knex.migrate.latest();
    await knex.seed.run();

    const models = requireFiles(stub.modelsPath);

    return {
        knex,
        models
    };
};
