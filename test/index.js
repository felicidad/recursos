// NPM Modules
const Path = require('path');
const Fs = require('fs');
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');
const Hapi = require('@hapi/hapi');
const Hoek = require('@hapi/hoek');
const Joi = require('@hapi/joi');

// App Modules
const Recursos = require('..');
const Db = require('./db');


// Module Variables
const {
    experiment,
    it,
    before,
    after,
    beforeEach,
    afterEach
} = exports.lab = Lab.script();
const expect = Code.expect;

const stub = {

    queries: [
        'all',
        'allWhere',
        'delWhere',
        'count',
        'countWhere',
        'findById',
        'oneWhere',
        'updateOne',
        'delOne'
    ]
};

const msg = (...args) => args.join(' ');

experiment('Recursos',  () => {

    experiment('resource function',  () => {

        before(async () => {

            stub.server = Hapi.Server({ port: 4321 });

            await stub.server.register({
                plugin: Recursos,
                options: stub.options
            });

            stub.db = await Db();
        });

        after(async () => {

            await stub.db.knex.migrate.rollback()
        });

        it('does not allow creation if arguments are bad', () => {

            expect(stub.server.resource).to.be.a.function();

            const { User } = stub.db.models;

            expect(() => stub.server.resource(), 'no args').to.throw(Error, 'Name is required and must be a string');
            expect(() => stub.server.resource(User), 'model').to.throw(Error, 'Name is required and must be a string');
            expect(() => stub.server.resource(User, { plural: 'y', singular: 'u' }), 'model + bad conf').to.throw(Error, 'Name is required and must be a string');
            expect(() => stub.server.resource('User'), 'name').to.throw(Error, 'Model is required');
            expect(() => stub.server.resource('User', {}), 'name + obj').to.throw(Error, 'Model must be an objection model');
            expect(() => stub.server.resource('User', User), 'name + model').to.throw(Error, 'child "singular" fails because ["singular" is required]');
            expect(() => stub.server.resource('User', User, { shit: 'config' }), 'name + model + bad conf').to.throw(Error, 'child "singular" fails because ["singular" is required]');
        });

        it('creates resources for a model given a good config', () => {

            const { User } = stub.db.models;
            const config = {
                singular: 'user',
                plural: 'users',
                configs: {}
            };

            expect(() => stub.server.resource('User', User, config)).to.not.throw();
        });

        it('does not allow double instantiation of a resource', () => {

            const { User } = stub.db.models;
            const config = {
                singular: 'user',
                plural: 'users',
                configs: {}
            };

            expect(() => stub.server.resource('User', User, config)).to.throw(Error, 'resources for User have already been declared');
        });
    });

    experiment('resources function',  () => {

        beforeEach(async () => {

            stub.server = Hapi.Server({ port: 4321 });

            await stub.server.register({
                plugin: Recursos,
                options: stub.options
            });

            stub.db = await Db();
        });

        afterEach(async () => {

            await stub.db.knex.migrate.rollback()
        });

        it('does not allow instantiation with bad arguments', () => {

            expect(() => stub.server.resources()).to.throw(Error, 'Models is required');
            expect(() => stub.server.resources(stub.db.models.User)).to.throw(Error, 'Models cannot be a single objection model. Must pass an object of models.');
        });

        it('does not allow instantiation of unpaired configs', () => {

            // Failed configs
            expect(() => stub.server.resources(stub.db.models, {})).to.throw(Error, 'Each model requires a config of same name');
            expect(() => stub.server.resources(stub.db.models, { User: {} })).to.throw(Error, 'Each model requires a config of same name');
            expect(() => stub.server.resources(stub.db.models, { User: {}, Post: {} })).to.throw(Error, 'Each model requires a config of same name');
            expect(() => stub.server.resources(stub.db.models, { User: {}, Comment: {} })).to.throw(Error, 'Each model requires a config of same name');
            expect(() => stub.server.resources(stub.db.models, { Post: {}, Comment: {} })).to.throw(Error, 'Each model requires a config of same name');
        });

        it('iterates through models and and options into resource function', () => {

            // Successful config
            expect(() => stub.server.resources(stub.db.models, { User: {}, Post: {}, Comment: {} })).to.throw(Error, 'child "singular" fails because ["singular" is required]');
        });
    });

    experiment('query function', () => {

        before(async () => {

            stub.server = Hapi.Server({ port: 4321 });

            await stub.server.register({
                plugin: Recursos,
                options: stub.options
            });

            stub.db = await Db();

            const { User, Post } = stub.db.models;
            const configs = {
                User: {
                    singular: 'user',
                    plural: 'users',
                    configs: {}
                },
                Post: {
                    singular: 'post',
                    plural: 'posts',
                    configs: {}
                }
            };

            stub.server.resources({ User, Post }, configs);
        });

        after(async () => {

            await stub.db.knex.migrate.rollback()
        });

        it('provides a query interface for a model', () => {

        });
    });
});
