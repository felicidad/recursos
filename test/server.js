// NPM Modules
const Hapi = require('@hapi/hapi');

// App Modules
const Recursos = require('..');
const Db = require('./db');


const server = Hapi.Server({ port: 4321 });

const start = async function () {

    await server.register({
        plugin: Recursos,
        options: {

        }
    });

    db = await Db();

    server.resources(db.models, {
        Post: { singular: 'post', plural: 'posts' },
        Comment: { singular: 'comment', plural: 'comments' },
        User: { singular: 'user', plural: 'users' },
    });

    console.log(server.table().map(({ method, path }) => {

        return [method, path]
    }))
}

;

start();
