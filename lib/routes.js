const Joi = require('@hapi/joi');
const Hoek = require('@hapi/hoek');

const schemas = {

    /** Pagination query */
    paginate: Joi.object().keys({
        page: Joi.number().min(1),
        limit: Joi.number().min(1),
        orderByRaw: Joi.string(),
        orderBy: Joi.string(),
        order: Joi.string()
    }),

    /** Asserts ID is an object */
    id: Joi.object().keys({ id: Joi.number().min(1) }),

    /**
     * Allows where clause for basic searching endpoints
     *
     * @example
     *
     * { age: 5, type: 'cat' }
     * ['age', '<=', 5]
     */
    where: Joi.alternatives().try(
        Joi.object()
            .pattern(/.*/, [Joi.string(), Joi.number(), Joi.boolean()])
            .label('where object clause'),
        Joi.array()
            .items(Joi.string(), Joi.number(), Joi.boolean())
            .label('where arguments')
    ).required(),
    fieldValue: Joi.object().keys({
        field: Joi.string().required(),
        value: Joi.alternatives().try(
            Joi.string(),
            Joi.number(),
            Joi.boolean()
        )
    })
};

const generalValidation = {
    all: { query: schemas.paginate },
    byId: { params: schemas.id },
    payloadById: {
        params: schemas.id,
        payload: schemas.where
    },
    payload: { payload: schemas.where },
    updateWhere: {
        payload: {
            where: schemas.where,
            changeset: Joi.object().required()
        }
    },
    byField: {
        params: schemas.fieldValue
    }
};

const defaultConfigurations = {
    singular: null,
    plural: null,
    options: {},
    validate: {},
    paths: {},
    events: {},
    returning: {},
    disable: []
};

const generatePath = (prefix, path) => (prefix ? [prefix, path].join('') : path);

const Crud = function (handlers, configurations = {}, opts = {}) {

    // Apply default configurations
    configurations = Hoek.applyToDefaults(Hoek.clone(defaultConfigurations), configurations);

    const event = this.event.bind(this);
    const { model } = opts;

    const { createSchema, updateSchema } = (model || {});

    let UpdateWhereSchema = false;
    let UpdateOneSchema = false;
    let InsertSchema = false;

    // If the model has an update schema, use it instead of any object
    if (updateSchema) {

        Hoek.assert(updateSchema.isJoi, 'expected model updateSchema to be a Joi object');

        UpdateWhereSchema = {

            payload: {

                ...generalValidation.updateWhere.payload,
                changeset: updateSchema
            }
        };

        UpdateOneSchema = {

            ...generalValidation.payloadById,
            payload: updateSchema
        };
    }

    // If the model has a create schema, use it instead of any object
    if (createSchema) {

        Hoek.assert(createSchema.isJoi, 'expected model updateSchema to be a Joi object');

        InsertSchema = {
            payload: Joi.alternatives().try(
                Joi.array().items(createSchema),
                createSchema
            )
        };
    }

    const {
        singular,
        plural,
        prefix,
        name,
        disable,
        options,
        validate,
        paths,
        events
    } = configurations;

    /**
     * This is going to generate all the routes with all the handlers
     * that will be needed. It configures validation per route, if applicable.
     * It creates and triggers events per route by default, if applicable.
     * You can also overwrite the defaults via `configuations.{options, validate, events, paths}`.
     * The following object, however, is where all the defaults are set.
     */
    const routeConfigs = {
        all: {
            method: 'GET',
            path: generatePath(prefix, `/${plural}`),
            defaultValidate: generalValidation.all,
            description: `Get all ${plural} - paginated`,
            tags: [plural, 'all'],
            handler: handlers.all
        },
        allWhere: {
            method: 'POST',
            path: generatePath(prefix, `/${plural}`),
            defaultValidate: {
                ...generalValidation.all,
                ...generalValidation.payload
            },
            description: `Get all ${plural} where filter by payload - paginated`,
            tags: [plural, 'allWhere'],
            handler: handlers.allWhere
        },
        updateWhere: {
            method: 'PUT',
            path: generatePath(prefix, `/${plural}`),
            description: `Update ${plural} where filter by payload`,
            defaultValidate: UpdateWhereSchema ? UpdateWhereSchema : generalValidation.updateWhere,
            tags: [plural, 'updateWhere'],
            handler: handlers.updateWhere,
            hasEvent: true
        },
        delWhere: {
            method: 'DELETE',
            path: generatePath(prefix, `/${plural}`),
            description: `Delete ${plural} where filter by payload`,
            defaultValidate: generalValidation.payload,
            tags: [plural, 'delWhere'],
            handler: handlers.delWhere,
            hasEvent: true
        },
        count: {
            method: 'GET',
            path: generatePath(prefix, `/${plural}/count`),
            description: `Count ${plural}`,
            defaultValidate: {},
            tags: [plural, 'count'],
            handler: handlers.count
        },
        countWhere: {
            method: 'POST',
            path: generatePath(prefix, `/${plural}/count`),
            description: `Count ${plural} where filter by payload`,
            defaultValidate: generalValidation.payload,
            tags: [plural, 'countWhere'],
            handler: handlers.countWhere
        },
        findById: {
            method: 'GET',
            path: generatePath(prefix, `/${singular}/{id}`),
            description: `Find ${singular} by ID`,
            defaultValidate: generalValidation.byId,
            tags: [plural, 'findById'],
            handler: handlers.findById
        },
        findByField: {
            method: 'GET',
            path: generatePath(prefix, `/${singular}/{field}/{value}`),
            description: `Find ${singular} by field value`,
            defaultValidate: generalValidation.byField,
            tags: [plural, 'findByField'],
            handler: handlers.findByField
        },
        oneWhere: {
            method: 'POST',
            path: generatePath(prefix, `/${singular}/where`),
            description: `Find ${singular} where filter by payload`,
            defaultValidate: generalValidation.payload,
            tags: [plural, 'oneWhere'],
            handler: handlers.oneWhere
        },
        insert: {
            method: 'POST',
            path: generatePath(prefix, `/${singular}`),
            description: `Create ${singular}. PostgreSQL accepts batch inserts.`,
            defaultValidate: InsertSchema ? InsertSchema : generalValidation.payload,
            tags: [plural, 'insert'],
            handler: handlers.insert,
            hasEvent: true
        },
        updateOne: {
            method: ['UPDATE', 'PATCH'],
            path: generatePath(prefix, `/${singular}/{id}`),
            description: `Update ${singular} by ID`,
            defaultValidate: UpdateOneSchema ? UpdateOneSchema : generalValidation.payloadById,
            tags: [plural, 'updateOne'],
            handler: handlers.updateOne,
            hasEvent: true
        },
        delOne: {
            method: 'DELETE',
            path: generatePath(prefix, `/${singular}/{id}`),
            description: `Delete ${singular} by ID`,
            defaultValidate: generalValidation.byId,
            tags: [plural, 'delOne'],
            handler: handlers.delOne,
            hasEvent: true
        }
    };

    /**
     * Creates the route and applies overrides to defaults
     * @param {string} key
     */
    const constructRoute = (key) => {

        const {
            method,
            path,
            description,
            tags,
            defaultValidate,
            handler,
            hasEvent
        } = routeConfigs[key];

        // Applies validation to all generated routes
        const globalValidate = validate['*'] || {};

        // Applies options to all generated routes
        const globalOptions = options['*'] || {};

        // Creates an event for all routes
        const globalEvents = events['*'];

        // Grab route options for key
        const _options = options[key];

        // Grab route validation for key
        const _validate = validate[key] || {};

        // Create the events if route has event
        if (hasEvent) {

            if (globalEvents === undefined && events[key] !== false) {

                event(`${name}:${key}`);
            }
            else if (globalEvents) {

                event(`${name}:${key}`);
            }
        }

        // Return the route config object
        return {
            method,
            path: paths[key] || path,
            options: {
                description,
                tags,
                validate: {
                    ...defaultValidate,
                    ...globalValidate,
                    ..._validate
                },
                ...globalOptions,
                ..._options
            },
            handler
        };
    };

    // Only create non-disabled routes
    return Object.keys(routeConfigs)
        .filter((key) => !disable.includes(key))
        .map((key) => constructRoute(key));
};

const Associations = function (handlers, configurations = {}, opts = {}) {

};

module.exports = {
    Crud
};
