const Boom = require('@hapi/boom');

const tryQuery = async function (...args) {

    const {
        key,
        run,
        emit,
        name
    } = args.shift();

    try {

        const results = await run(...args);

        await emit(`${name}:${key}`, {
            args,
            results
        });

        return results;
    }
    catch (e) {

        return Boom.serverUnavailable(e);
    }
};

const generateOptions = function (opts, queries, key) {

    console.log(this.realm);
    return {
        key,
        run: queries[key],
        emit: this.queryEmit.bind(this),
        ...opts
    };
};

const Crud = (queries, opts) => {

    const options = generateOptions.bind(this, opts, queries);

    return {
        all: async ({ query }, h) => await tryQuery(options('all'), query),
        allWhere: async ({ payload }, h) => await tryQuery(options('allWhere'), payload),
        updateWhere: async ({ payload }, h) => await tryQuery(options('updateWhere'), payload),
        delWhere: async ({ payload }, h) => await tryQuery(options('delWhere'), payload),
        count: async () => await tryQuery(options('count')),
        countWhere: async ({ payload }, h) => await tryQuery(options('countWhere'), payload),
        findById: async ({ params }, h) => await tryQuery(options('findById'), params.id) || Boom.notFound(`${opts.singular} not found`),
        findByField: async ({ params }, h) => await tryQuery(options('findByField'), params) || Boom.notFound(`${opts.singular} not found`),
        oneWhere: async ({ payload }, h) => await tryQuery(options('oneWhere'), payload) || Boom.notFound(`${opts.singular} not found`),
        insert: async ({ payload }, h) => await tryQuery(options('insert'), payload),
        updateOne: async ({ params, payload }, h) => await tryQuery(options('update'), params.id, payload) || Boom.notFound(`${opts.singular} not found`),
        delOne: async ({ params }, h) => await tryQuery(options('delOne'), params.id) || Boom.notFound(`${opts.singular} not found`)
    };
};

const Associations = (queries, opts) => {};


module.exports = { Crud, Associations };
