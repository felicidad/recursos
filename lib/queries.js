
/**
 * Receives a query and a pagination payload
 * Can order by fields, or custom sql order query
*/
const pager = (q, { page, limit, orderBy, orderByRaw, order }) => {

    const hasPaginate = page !== undefined;
    const hasSort = !!orderBy || !!orderByRaw;

    if (hasSort) {

        if (!!orderByRaw) {

            q = q.orderByRaw(orderByRaw);
        }
        else {

            let sortArgs = [orderBy];
            const sortIsArray = orderBy.constructor === Array;

            if (sortIsArray) {

                sortArgs = [sortArgs];
            }
            else if (!sortIsArray && !!order) {

                sortArgs.push(order);
            }

            q = q.orderBy(...sortArgs);
        }
    }

    if (hasPaginate) {

        q = q.page(page, limit);
    }

    return q;
};

/**
 * Checks whether to return or not
 * Returning only works on postgres, and sometimes on mysql
 */
// eslint-disable-next-line arrow-body-style
const update = (q, args, returning) => {

    return returning
        ? q.update(...args).returning('*')
        : q.update(...args);
};

/**
 * Checks whether to return or not
 * Returning only works on postgres, and sometimes on mysql
 */
// eslint-disable-next-line arrow-body-style
const insert = (q, args, returning) => {

    return returning
        ? q.insert(...args).returning('*')
        : q.insert(...args);
};

/** CRUD functionality for models */
const Crud = (Model, opts = {}) => ({

    async all(paginate = false) {

        if (paginate) {
            return await pager(
                Model.query().select(),
                paginate
            );
        }

        return await Model.query().select();
    },

    async allWhere(where, paginate = false) {

        const isArray = where.constructor === Array;
        const whereArgs = isArray ? [...where] : [where];

        if (paginate) {
            return await pager(
                Model.query().select().where(...whereArgs),
                paginate
            );
        }

        return await Model.query().select().where(...whereArgs);
    },

    async updateWhere({ where, changeset }, returnFields = false) {

        const isArray = where.constructor === Array;
        const returning = opts.updateWhere ? opts.updateWhere.returning : opts.returning;

        const whereArgs = isArray ? [...where] : [where];
        return await update(Model.query().select().where(...whereArgs), [changeset], returnFields || returning);
    },

    async delWhere(where) {

        const isArray = where.constructor === Array;
        const whereArgs = isArray ? [...where] : [where];

        return await Model.query().where(...whereArgs).del()
    },

    async count() {

        return await Model.query().count('* as total').first();
    },

    async countWhere(where) {

        const isArray = where.constructor === Array;
        const whereArgs = isArray ? [...where] : [where];

        return await Model.query().where(...whereArgs).count('* as total').first();
    },

    async findById(id) {

        return await Model.query().findById(id);
    },

    async findByField({ field, value }) {

        return await Model.query().select().where({ [field]: value });
    },

    async oneWhere(where) {

        const isArray = where.constructor === Array;
        const whereArgs = isArray ? [...where] : [where];

        return await Model.query().select().where(...whereArgs).first();
    },

    /**
     * Batch insert works in postgres
     * Payload can be an array of objects
     */
    async insert(payload, returnFields = false) {

        const returning = opts.insert ? opts.insert.returning : opts.returning;
        return await insert(Model.query(), [payload], returnFields || returning);
    },

    async updateOne(id, changeset, returnFields = false) {

        const returning = opts.updateOne ? opts.updateOne.returning : opts.returning;
        return await update(Model.query().where({ id }), [changeset], returnFields || returning);
    },

    async delOne(id) {

        return await Model.query().where({ id }).del();
    }
});


/** CRUD functionality for model associations */
const Associations = (modelInstance, association) => ({

    async all() {

        return await modelInstance.$relatedQuery(association);
    },

    async findById(id) {

        return await modelInstance.$relatedQuery(association).findById(id);
    },

    async allWhere(where) {

        return await modelInstance.$relatedQuery(association).where(where);
    },

    async oneWhere(where) {

        return await modelInstance.$relatedQuery(association).where(where).first();
    },

    async update(id, changeset) {

        return await modelInstance.$relatedQuery(association).where({ id }).update(changeset);
    },

    async delOne(id) {

        return await modelInstance.$relatedQuery(association).where({ id }).delete();
    },

    async delWhere(where) {

        return await modelInstance.$relatedQuery(association).where(where).delete();
    },

    async count() {

        return await modelInstance.$relatedQuery(association).count();
    },

    async countWhere(where) {

        return await modelInstance.$relatedQuery(association).where(where).count();
    },

    async associate(ids) {

        return await modelInstance.$relatedQuery(association).relate(ids);
    },

    async disassociate(ids) {

        return await modelInstance.$relatedQuery(association).unrelate(ids);
    }
});

module.exports = { Crud, Associations };
