const Hoek = require('@hapi/hoek');

module.exports = (store) => {

    // Generates a relationship key, `post:comments`
    const relationKey = (n, r) => `${n}:${r}`;

    // Adds model to models map. Does not allow model to be
    // double instantiated..
    const loadModel = (name, model) => {

        Hoek.assert(!store.models.has(name), `resources for ${name} have already been declared`);
        setModel(name, model);
    };

    // Sets model by name and notes it's relationships
    const setModel = (name, model) => {

        const relations = Object.keys(model.relationMappings || {});

        store.models.set(name, {
            model,
            relations
        });
    };

    // Sets model by name and notes it's relationships
    const getModel = (name) => store.models.get(name);

    // Sets a query by name to query map
    const setQueries = (name, queries) => {

        store.queries.set(name, queries);
    };

    // Sets relational query to query map
    const setRelationalQueries = (name, relation, queries) => {

        store.queries.set(relationKey(name, relation), queries);
    };

    // Gets query from query map
    const getQuery = (name, query) => {

        Hoek.assert(store.models.has(name), `${name} has not been added as a resource`);
        return store.queries.get(name)[query];
    };

    // Gets relational query from query map
    const getRelationalQuery = (name, relation, query) => {

        Hoek.assert(store.queries.has(relationKey(name, relation), `${relation} has not been added as a relationship of ${name}`));

        return store.queries.get(relationKey(name, relation))[query];
    };

    /**
     * Runs a stored query
     *
     * @param  {string} name Model name
     * @param  {string} action Query action
     * @param  {...any} args Arguments to pass to query
     *
     * @example
     * runQuery('Post', 'allWhere', ['created_at', '<=', new Date(+new Date - 24 * 3600000)])
     */
    const runQuery = (...args) => {

        const name = args.shift();
        const action = args.shift();

        const query = getQuery(name, action);

        Hoek.assert(query, `${name} ${action} does not exist`);

        return query(...args);
    };

    /**
     * Runs a stored relational query
     *
     * @param  {string} name Model name
     * @param  {string} relation Relationship name
     * @param  {string} action Query action
     * @param  {...any} args Arguments to pass to query
     *
     * @example
     * runQuery('Post', 'comments', 'create', { body: 'test'});
     */
    const runRelationalQuery = (...args) => {

        const name = args.shift();
        const relation = args.shift();
        const action = args.shift();

        const query = getRelationalQuery(name, relation);

        Hoek.assert(query, `${name} ${relation} ${action} does not exist`);

        return query(...args);
    };


    return {
        relationKey,
        loadModel,
        setModel,
        setQueries,
        setRelationalQueries,
        getModel,
        getQuery,
        getRelationalQuery,
        runQuery,
        runRelationalQuery
    };
}
;