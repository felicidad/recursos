const Joi = require('@hapi/joi');
const Hoek = require('@hapi/hoek');

const Handlers = require('./handlers');
const Queries = require('./queries');
const Routes = require('./routes');

const queries = [
    'all',
    'allWhere',
    'updateWhere',
    'delWhere',
    'count',
    'countWhere',
    'findById',
    'findByField',
    'oneWhere',
    'insert',
    'updateOne',
    'delOne'
];

const QueriesObject = Joi.object().pattern(
    Joi.string().only(queries),
    [Joi.any()]
);

const ConfigsSchema = Joi.object().keys({
    singular: Joi.string().required().min(1),
    plural: Joi.string().required().min(1),
    configs: QueriesObject.optional(),
    validate: QueriesObject.optional(),
    paths: QueriesObject.optional(),
    events: QueriesObject.optional(),
    disable: Joi.array().allow(queries).optional()
});

const OptsSchema = Joi.object().keys({
    context: Joi.object()
});

module.exports = function (name, model, configs = {}, opts = {}) {

    Hoek.assert(name && typeof name === 'string', 'Name is required and must be a string');
    Hoek.assert(model, 'Model is required');
    Hoek.assert(typeof model.query === 'function', 'Model must be an objection model');

    let valid = ConfigsSchema.validate(configs);

    Hoek.assert(!valid.error, valid.error);

    valid = OptsSchema.validate(opts);

    Hoek.assert(!valid.error, valid.error);

    const {
        loadModel,
        getModel,
        setQueries,
        setRelationalQueries
    } = this['recursos:store']();

    loadModel(name, model);
    configs.name = name;

    // Create CRUD queries
    const crudQueries = Queries.Crud.bind(this)(model, configs, opts);

    // Save them to store for later use
    setQueries(name, crudQueries);

    // Create Handlers
    const crudHandlers = Handlers.Crud.bind(this)(crudQueries, configs, opts);

    // Create route configs
    const crudRoutes = Routes.Crud.bind(this)(crudHandlers, configs, {
        ...opts,
        model
    });

    // Queries.Associations.bind(this)(model, configs, opts);
    const { relations } = getModel(name);

    // Set relational queries
    relations.map((relation) => {

        // First, extract from queries
        return [
            relation,
            Queries.Associations.bind(this)(model, relation)
        ];
    }).map(([relation, queries]) => {

        // Then, set them to query map
        setRelationalQueries(name, relation, queries);
    });

    // TODO:
    // const relationalHandlers;
    // const relationalRoutes;



    (opts.context || this).route(crudRoutes);

};
