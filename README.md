# CRUD Generator for Hapi / Objection / Knex

Under construction


TODO:
- Create association endpoints and handlers
- Move more options into models object, such as `plural` and `singular`
- Use queries in store instead of passing to handlers
- Create decorator for server / request that exposes the useful parts of `store` for developer use; abstract internal methods away from store.